package com.example.jsfdemo.web;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

@ApplicationScoped
@Named("hobbiesBean")
public class Hobbies {

	private List<SelectItem> hobbyItems;
	private String[] hobbies = {
			"programowanie",
			"czytanie dokumentacji",
			"debuggowanie apl.web."};

	{
		hobbyItems = new ArrayList<SelectItem>();
		for(String hobby:hobbies){
			hobbyItems.add(new SelectItem(hobby,hobby));
		}
	}
	
	public List<SelectItem> getHobbyItems() {
		return hobbyItems;
	}
	
}
