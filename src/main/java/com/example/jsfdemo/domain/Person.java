package com.example.jsfdemo.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.model.SelectItem;
import javax.validation.constraints.Min;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Person {
	
	//logi trafiają do /opt/devel/glassfish3/glassfish/domains/domain1/logs/server.log
	private Logger logger = Logger.getLogger(getClass().getName());
	
	private String firstName = "unknown";
	private String zipCode = "11-222";
	private String pin = "13342534535";
	private Date dateOfBirth = new Date();
	private double weight = 50;
	private boolean married;
	private int numOfChildren;
	private List<String> hobbies = new ArrayList<String>();
	
	@Size(min = 2, max = 20)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Pattern(regexp = "[0-9]{2}-[0-9]{3}")
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	@Size(min = 2)
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	
	@Min(0)
	public int getNumOfChildren() {
		return numOfChildren;
	}
	public void setNumOfChildren(int numOfChildren) {
		this.numOfChildren = numOfChildren;
	}
	
	@Past
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public boolean isMarried() {
		return married;
	}
	public void setMarried(boolean married) {
		this.married = married;
	}
	
	public List<String> getHobbies() {
		return hobbies;
	}
	
	public void setHobbies(List<String> hobbies) {
		logger.info("jep, wywołano" + hobbies.size());
		logger.info(Arrays.toString(hobbies.toArray()));
		this.hobbies = hobbies;
	}
	
	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", zipCode=" + zipCode
				+ ", pin=" + pin + ", dateOfBirth=" + dateOfBirth + ", weight="
				+ weight + ", married=" + married + ", numOfChildren="
				+ numOfChildren + ", hobbies=" + Arrays.toString(hobbies.toArray()) + "]";
	}

	
}

